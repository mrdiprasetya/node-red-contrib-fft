module.exports = function(RED) {
    var FFT = require('dsp.js').FFT;

    function fft(config) {
        RED.nodes.createNode(this,config);
        var node = this;
        node.on('input', function(msg) {
	    // Do some FFT stuff
        var signal = msg.payload;
	    var fft = new FFT(16384, 3200);
	    fft.forward(signal);

	    msg.payload = fft.spectrum;
            node.send(msg);
        });
    }
    RED.nodes.registerType("fft",fft);
}

